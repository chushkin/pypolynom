Template
========

polynom project solve equiation. :math:`ax^2+bx+c=0`

Look how easy it is to use:

    import pypolynom
    # Get your stuff done
    pypolynom.polynom()

Features
--------

- Be awesome
- Make things faster

Installation
------------

Install pypolynom by running:

    install pypolynom

Contribute
----------

- Issue Tracker: github.com/pypolynom/pypolynom/issues
- Source Code: github.com/pypolynom/pypolynom

Support
-------

If you are having issues, please let us know.
We have a mailing list located at: googoo@gigi.com

License
-------

The project is licensed under the MIT license.